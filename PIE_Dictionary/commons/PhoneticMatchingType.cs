﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_Dictionary.commons
{
    class PhoneticMatchingType
    {
        public static readonly PhoneticMatchingType DIAERESIS = new PhoneticMatchingType();
        public static readonly PhoneticMatchingType EPENTHESIS = new PhoneticMatchingType();
        public static readonly PhoneticMatchingType METATHESIS = new PhoneticMatchingType();
        public static readonly PhoneticMatchingType REGULAR = new PhoneticMatchingType();
        public static readonly PhoneticMatchingType IRREGULAR = new PhoneticMatchingType();

        public static IEnumerable<PhoneticMatchingType> Values
        {
            get
            {
                yield return DIAERESIS;
                yield return EPENTHESIS;
                yield return METATHESIS;
                yield return REGULAR;
                yield return IRREGULAR;
            }
        }

        private PhoneticMatchingType()
        {

        }

        public bool isRegular()
        {
            return this == REGULAR;
        }
    }
}