﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_Dictionary.commons
{
    public class ExcelDictionaryColumn
    {
        public static readonly ExcelDictionaryColumn ENTRY_NUMBER = new ExcelDictionaryColumn("A", 0);
        public static readonly ExcelDictionaryColumn LINE_NUMBER = new ExcelDictionaryColumn("B", 1);
        public static readonly ExcelDictionaryColumn LEMMA = new ExcelDictionaryColumn("C", 2);
        public static readonly ExcelDictionaryColumn LANGUAGE = new ExcelDictionaryColumn("D", 3);
        public static readonly ExcelDictionaryColumn RECONSTRUCTED_FORM = new ExcelDictionaryColumn("E", 4);
        public static readonly ExcelDictionaryColumn VARIANT = new ExcelDictionaryColumn("F", 5);
        public static readonly ExcelDictionaryColumn ZERO_POSITION = new ExcelDictionaryColumn("G", 6);
        public static readonly ExcelDictionaryColumn POSITION_1 = new ExcelDictionaryColumn("H", 7);
        public static readonly ExcelDictionaryColumn POSITION_2 = new ExcelDictionaryColumn("I", 8);
        public static readonly ExcelDictionaryColumn POSITION_3 = new ExcelDictionaryColumn("J", 9);
        public static readonly ExcelDictionaryColumn POSITION_4 = new ExcelDictionaryColumn("K", 10);
        public static readonly ExcelDictionaryColumn POSITION_5 = new ExcelDictionaryColumn("L", 11);
        public static readonly ExcelDictionaryColumn POSITION_6 = new ExcelDictionaryColumn("M", 12);
        public static readonly ExcelDictionaryColumn POSITION_7 = new ExcelDictionaryColumn("N", 13);
        public static readonly ExcelDictionaryColumn POSITION_8 = new ExcelDictionaryColumn("O", 14);
        public static readonly ExcelDictionaryColumn POSITION_9 = new ExcelDictionaryColumn("P", 15);
        public static readonly ExcelDictionaryColumn POSITION_10 = new ExcelDictionaryColumn("Q", 16);
        public static readonly ExcelDictionaryColumn POSITION_11 = new ExcelDictionaryColumn("R", 17);
        public static readonly ExcelDictionaryColumn POSITION_0 = new ExcelDictionaryColumn("S", 18);
        public static readonly ExcelDictionaryColumn RECONSTRUCTED_MEANING = new ExcelDictionaryColumn("T", 19);
        public static readonly ExcelDictionaryColumn COMMENT = new ExcelDictionaryColumn("U", 20);
        public static readonly ExcelDictionaryColumn LINK = new ExcelDictionaryColumn("V", 21);

        private string column;
        private int number;

        public static IEnumerable<ExcelDictionaryColumn> Values
        {
            get
            {
                yield return ENTRY_NUMBER;
                yield return LINE_NUMBER;
                yield return LEMMA;
                yield return LANGUAGE;
                yield return RECONSTRUCTED_FORM;
                yield return VARIANT;
                yield return ZERO_POSITION;
                yield return POSITION_1;
                yield return POSITION_2;
                yield return POSITION_3;
                yield return POSITION_4;
                yield return POSITION_5;
                yield return POSITION_6;
                yield return POSITION_7;
                yield return POSITION_8;
                yield return POSITION_9;
                yield return POSITION_10;
                yield return POSITION_11;
                yield return POSITION_0;
                yield return RECONSTRUCTED_MEANING;
                yield return COMMENT;
                yield return LINK;
            }
        }

        public string Column
        {
            get
            {
                return column;
            }

            set
            {
                column = value;
            }
        }

        public int Number
        {
            get
            {
                return number;
            }

            set
            {
                number = value;
            }

        }
        private ExcelDictionaryColumn(string column, int number)
        {
            this.Column = column;
            this.Number = number;
        }

    }
}
