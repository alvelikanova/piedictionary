﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_Dictionary.commons
{
    public class LanguagesEnum
    {
        public static readonly LanguagesEnum PIE = new LanguagesEnum("PIE", "ПИЕ*", "*ПИЕ");
        public static readonly LanguagesEnum AVESTAN = new LanguagesEnum("Ave", "Av");
        public static readonly LanguagesEnum ALBAN = new LanguagesEnum("Alb");
        public static readonly LanguagesEnum ENGLISH = new LanguagesEnum("Eng");
        public static readonly LanguagesEnum ARMENIAN = new LanguagesEnum("Arm", "Arm.");
        public static readonly LanguagesEnum BENGAL = new LanguagesEnum("Ben");
        public static readonly LanguagesEnum BEOTIC = new LanguagesEnum("Beot");
        public static readonly LanguagesEnum BRETON = new LanguagesEnum("Bre");
        public static readonly LanguagesEnum CYMRAEG = new LanguagesEnum("Cym");
        public static readonly LanguagesEnum VEDIC = new LanguagesEnum("San");
        public static readonly LanguagesEnum GESSIC = new LanguagesEnum("Gess");
        public static readonly LanguagesEnum GOTHIC = new LanguagesEnum("Got");
        public static readonly LanguagesEnum OLD_GREEK = new LanguagesEnum("Ell");
        public static readonly LanguagesEnum DANISH = new LanguagesEnum("Dan");
        public static readonly LanguagesEnum DORICH = new LanguagesEnum("Dor");
        public static readonly LanguagesEnum OLD_ENGLISH = new LanguagesEnum("Ang");
        public static readonly LanguagesEnum OLD_CYMRAEG = new LanguagesEnum("Owl");
        public static readonly LanguagesEnum OLD_IRISH = new LanguagesEnum("Sga");
        public static readonly LanguagesEnum OLD_ICELANDIC = new LanguagesEnum("Non");
        public static readonly LanguagesEnum OLD_LATVIAN = new LanguagesEnum("Olat");
        public static readonly LanguagesEnum OLD_LITHUANIAN = new LanguagesEnum("Olit");
        public static readonly LanguagesEnum OLD_GERMAN = new LanguagesEnum("Goh");
        public static readonly LanguagesEnum OLD_IRAN = new LanguagesEnum("Peo");
        public static readonly LanguagesEnum OLD_RUSSIAN = new LanguagesEnum("Orv");
        public static readonly LanguagesEnum OLD_SAXON = new LanguagesEnum("Osx");
        public static readonly LanguagesEnum OLD_FRENCH = new LanguagesEnum("Fro");
        public static readonly LanguagesEnum OLD_FRIZ = new LanguagesEnum("Ofs");
        public static readonly LanguagesEnum IRISH = new LanguagesEnum("Gle", "Ir.");
        public static readonly LanguagesEnum ICELANDIC = new LanguagesEnum("Isl");
        public static readonly LanguagesEnum SPANISH = new LanguagesEnum("Spa");
        public static readonly LanguagesEnum ITALIAN = new LanguagesEnum("Ita");
        public static readonly LanguagesEnum CORN = new LanguagesEnum("Cor");
        public static readonly LanguagesEnum KURSH = new LanguagesEnum("Xcu");
        public static readonly LanguagesEnum LATIN = new LanguagesEnum("Lat", "Lat.");
        public static readonly LanguagesEnum LATVIAN = new LanguagesEnum("Lav", "Latv.");
        public static readonly LanguagesEnum LITHUANIAN = new LanguagesEnum("Lit", "Li.");
        public static readonly LanguagesEnum SORBIAN = new LanguagesEnum("Wen");
        public static readonly LanguagesEnum GERMAN = new LanguagesEnum("Deu");
        public static readonly LanguagesEnum NORWEGIAN = new LanguagesEnum("Nor");
        public static readonly LanguagesEnum OSETIN = new LanguagesEnum("Oss");
        public static readonly LanguagesEnum PEHLEVI = new LanguagesEnum("Pal");
        public static readonly LanguagesEnum ROMANIAN = new LanguagesEnum("Ron");
        public static readonly LanguagesEnum SANSKRIT = new LanguagesEnum("Skr");
        public static readonly LanguagesEnum SARDIN = new LanguagesEnum("Srd");
        public static readonly LanguagesEnum SINDHI = new LanguagesEnum("Snd");
        public static readonly LanguagesEnum SLOVAK = new LanguagesEnum("Slk");
        public static readonly LanguagesEnum OLD_SLAVONIC = new LanguagesEnum("Chu");
        public static readonly LanguagesEnum TOCHAR = new LanguagesEnum("Toch");
        public static readonly LanguagesEnum HITT = new LanguagesEnum("Hit");
        public static readonly LanguagesEnum HINDI = new LanguagesEnum("Hin");
        public static readonly LanguagesEnum EOLIC = new LanguagesEnum("Eol");
        public static readonly LanguagesEnum FRENCH = new LanguagesEnum("Fra");
        public static readonly LanguagesEnum UKRANIAN = new LanguagesEnum("Ukr");
        public static readonly LanguagesEnum UMBR = new LanguagesEnum("Xum");
        public static readonly LanguagesEnum CHECH = new LanguagesEnum("Ces", "Cz.");
        public static readonly LanguagesEnum OLD_CHURCH_SLAVONIC = new LanguagesEnum("OCS");

        private string signature;
        private string[] variants;

        public static IEnumerable<LanguagesEnum> Values
        {
            get
            {
                yield return PIE;
                yield return AVESTAN;
                yield return ALBAN;
                yield return ENGLISH;
                yield return ARMENIAN;
                yield return BENGAL;
                yield return BEOTIC;
                yield return BRETON;
                yield return CYMRAEG;
                yield return VEDIC;
                yield return GESSIC;
                yield return GOTHIC;
                yield return OLD_GREEK;
                yield return DANISH;
                yield return DORICH;
                yield return OLD_ENGLISH;
                yield return OLD_CYMRAEG;
                yield return OLD_IRISH;
                yield return OLD_ICELANDIC;
                yield return OLD_LATVIAN;
                yield return OLD_LITHUANIAN;
                yield return OLD_GERMAN;
                yield return OLD_IRAN;
                yield return OLD_RUSSIAN;
                yield return OLD_SAXON;
                yield return OLD_FRENCH;
                yield return OLD_FRIZ;
                yield return IRISH;
                yield return ICELANDIC;
                yield return SPANISH;
                yield return ITALIAN;
                yield return CORN;
                yield return KURSH;
                yield return LATIN;
                yield return LATVIAN;
                yield return LITHUANIAN;
                yield return SORBIAN;
                yield return GERMAN;
                yield return NORWEGIAN;
                yield return OSETIN;
                yield return PEHLEVI;
                yield return ROMANIAN;
                yield return SANSKRIT;
                yield return SARDIN;
                yield return SINDHI;
                yield return SLOVAK;
                yield return OLD_SLAVONIC;
                yield return TOCHAR;
                yield return HITT;
                yield return HINDI;
                yield return EOLIC;
                yield return FRENCH;
                yield return UKRANIAN;
                yield return UMBR;
                yield return CHECH;
                yield return OLD_CHURCH_SLAVONIC;
            }
        }
        public string Signature
        {
            get
            {
                return signature;
            }

            set
            {
                signature = value;
            }
        }
        public string[] Variants
        {
            get
            {
                return variants;
            }

            set
            {
                variants = value;
            }
        }

        public LanguagesEnum(string signature, params string[] variants)
        {
            Signature = signature;
            Variants = variants;
        }

        public static LanguagesEnum parseSignature(string signature)
        {
            LanguagesEnum possibleLanguage = null;
            foreach (LanguagesEnum language in Values)
            {
                string languageSignature = language.Signature;
                if (languageSignature.Equals(signature))
                {
                    possibleLanguage = language;
                }
                else
                {
                    string[] languageVariants = language.Variants;
                    foreach (string languageVariant in languageVariants)
                    {
                        if (signature.Equals(languageVariant))
                        {
                            possibleLanguage = language;
                        }
                    }
                }
            }
            return possibleLanguage;
        }
    }
}