﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Win32;
using PIE_Dictionary.services;
using PIE_Dictionary.data;
using PIE_Dictionary.utils;
using PIE_Dictionary.ui.model;
using PIE_Dictionary.ui.components;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace PIE_Dictionary
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // services
        private PIEDataImportService pieDataImport;
        private PIEDataService pieDataService;

        public MainWindow()
        {
            InitializeComponent();
            pieDataImport = new PIEDataImportService();
            pieDataService = new PIEDataService();
            FillDataGrid();
        }

        private void LoadDictionaryFromFileBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel (xls, .xlsx)|*.xls; *.xlsx";
            openFileDialog.CheckPathExists = true;
            openFileDialog.Multiselect = true;

            Nullable<bool> result = openFileDialog.ShowDialog();

            if (result == true)
            {
                string[] filenames = openFileDialog.FileNames;

                EnableProcessBar();

                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.DoWork += worker_LoadDictionary;
                worker.ProgressChanged += worker_ProgressChanged;
                worker.ProgressChanged += null;
                worker.RunWorkerAsync(filenames);
            }
        }

        private void EnableProcessBar()
        {
            mainArea.IsEnabled = false;
            operationProgress.Visibility = Visibility.Visible;
            operationProgress.Value = 0;
            operationLabel.Visibility = Visibility.Visible;
        }
        private void DisableProcessBar()
        {
            mainArea.IsEnabled = true;
            operationProgress.Visibility = Visibility.Hidden;
            operationProgress.Value = 0;
            operationLabel.Visibility = Visibility.Hidden;
            operationLabel.Content = null;
        }
        private void worker_LoadDictionary(object sender, DoWorkEventArgs e)
        {
            string[] filenames = e.Argument as string[];
            pieDataImport.LoadDictionaryEntriesFromExcelFiles(sender, filenames);
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            operationProgress.Value = e.ProgressPercentage;
            PIEDataImportStage stage = (e.UserState as PIEDataImportStage);
            if (stage == PIEDataImportStage.PIE_COMPLETED)
            {
                DisableProcessBar();
                FillDataGrid();
            }
            else
            {
                operationLabel.Content = stage.Message;
            }
        }

        void FillDataGrid()
        {
            dictionaryDataGrid.DataContext = null;
            IEnumerable<Vocabulary_entry> entries = pieDataService.LoadVocabularyEntries();
            if (entries != null)
            {
                ObservableCollection<VocabularyEntry> vocabularyEntriesCollection = new ObservableCollection<VocabularyEntry>();
                foreach (Vocabulary_entry entry in entries)
                {
                    string lemma = entry.Lemma;
                    if (entry.Number_vocabulary_entry.HasValue && !String.IsNullOrEmpty(lemma))
                    {
                        int number = entry.Number_vocabulary_entry.Value;
                        vocabularyEntriesCollection.Add(new VocabularyEntry { Number = number, Lemma = lemma });
                    }
                }
                dictionaryDataGrid.DataContext = vocabularyEntriesCollection;
            }
        }

        private void showDictionaryConformitiesBtnClick(object sender, RoutedEventArgs e)
        {
            VocabularyEntry vocabularyEntry = ((Button)sender).CommandParameter as VocabularyEntry;
            VocabularyEntryWindow dialog = new VocabularyEntryWindow(vocabularyEntry);
            dialog.Show();
            dialog.ShowActivated = true;
        }

        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            VocabularySearchWindow window = new ui.components.VocabularySearchWindow();
            window.Show();
            window.ShowActivated = true;
        }

        private void SplitMergeBtn_Click(object sender, RoutedEventArgs e)
        {
            PIEUtils util = new PIEUtils();
            util.DictionarySplitMerge();
        }
    }
}