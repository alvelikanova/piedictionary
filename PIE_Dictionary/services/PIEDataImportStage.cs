﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_Dictionary.services
{
    class PIEDataImportStage
    {
        public static readonly PIEDataImportStage PIE_IMPORT = new PIEDataImportStage("Получение данных...");
        public static readonly PIEDataImportStage PIE_PROCESSING = new PIEDataImportStage("Обработка данных...");
        public static readonly PIEDataImportStage PIE_SAVING = new PIEDataImportStage("Сохранение в базу данных...");
        public static readonly PIEDataImportStage PIE_COMPLETED = new PIEDataImportStage(null);

        private string message;
        public static IEnumerable<PIEDataImportStage> Values
        {
            get
            {
                yield return PIE_IMPORT;
                yield return PIE_PROCESSING;
                yield return PIE_SAVING;
                yield return PIE_COMPLETED;
            }
        }
        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
            }
        }
        private PIEDataImportStage(string message)
        {
            this.Message = message;
        }
    }
}