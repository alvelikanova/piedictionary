﻿using System;
using System.Collections.Generic;
using PIE_Dictionary.commons;
using PIE_Dictionary.repositories;
using PIE_Dictionary.data;
using PIE_Dictionary.utils;
using System.Data;

namespace PIE_Dictionary.services
{
    class PIEDataImportService
    {
        private const int ACTIVE_SHEET = 1;
        private const int FIRST_ROW = 1;
        private const int OPERATION_STARTED = 0;
        private const int OPERATION_COMPLETED = 100;
        private static UnitOfWork unitOfWork;
        private static IEnumerable<Language> possibleLanguages;
        public PIEDataImportService()
        {
            unitOfWork = new UnitOfWork();
            possibleLanguages = unitOfWork.LanguageRepository.Get();
        }
        public void LoadDictionaryEntriesFromExcelFiles(object sender, string[] filenames)
        {
            if (filenames == null || filenames.Length == 0)
            {
                return;
            }

            BackgroundWorkerUtils.ReportProgress(sender, OPERATION_STARTED, PIEDataImportStage.PIE_IMPORT);

            IList<DataTable> dataTables = new List<DataTable>();
            foreach (string filename in filenames)
            {
                if (IOUtils.CheckFilepathIsValid(filename))
                {
                    DataTable dataTable = ExcelUtils.LoadDataTableFromExcel(filename);
                    dataTables.Add(dataTable);
                }
            }

            int totalRowCount = 0;
            int totalRows = CalculateTotalRows(dataTables);
            SortedList<int, Vocabulary_entry> entries = new SortedList<int, Vocabulary_entry>();
            foreach (DataTable dataTable in dataTables)
            {
                int columns = dataTable.Columns.Count;
                int rows = dataTable.Rows.Count;
                for (int row = FIRST_ROW; row < rows; row++)
                {
                    totalRowCount++;
                    ProcessDictionaryTableRow(row, dataTable, entries);
                    int completed = BackgroundWorkerUtils.GetProgress(totalRows, totalRowCount);
                    BackgroundWorkerUtils.ReportProgress(sender, completed, PIEDataImportStage.PIE_PROCESSING);
                }
            }
            BackgroundWorkerUtils.ReportProgress(sender, OPERATION_COMPLETED, PIEDataImportStage.PIE_SAVING);
            LoadDictionaryToDatabase(entries.Values);
            BackgroundWorkerUtils.ReportProgress(sender, OPERATION_COMPLETED, PIEDataImportStage.PIE_COMPLETED);
        }
        private int CalculateTotalRows(IList<DataTable> dataTables)
        {
            int result = 0;
            if (dataTables != null && dataTables.Count > 0)
            {
                foreach (DataTable dataTable in dataTables)
                {
                    result += dataTable.Rows.Count;
                }
            }
            return result;
        }
        private void LoadDictionaryToDatabase(IList<Vocabulary_entry> dictionaryEntries)
        {
            foreach (Vocabulary_entry vocabularyEntry in dictionaryEntries)
            {
                if (!CheckVocabularyEntryExists(vocabularyEntry))
                {
                    unitOfWork.VocabularyEntryRepository.Insert(vocabularyEntry);
                    ICollection<Dictionary_conformity> conformities = vocabularyEntry.Dictionary_conformity;
                    if (conformities != null && conformities.Count > 0)
                    {
                        foreach (Dictionary_conformity dictionaryConformity in conformities)
                        {
                            if (!CheckDictionaryConformityExists(dictionaryConformity))
                            {
                                unitOfWork.DictionaryConformityRepository.Insert(dictionaryConformity);
                            }
                        }
                    }
                }
            }
            unitOfWork.Save();
        }
        private bool CheckVocabularyEntryExists(Vocabulary_entry vocabularyEntry)
        {
            IEnumerable<Vocabulary_entry> entries = unitOfWork.VocabularyEntryRepository.Get(x => x.Number_vocabulary_entry == vocabularyEntry.Number_vocabulary_entry);
            if (entries != null)
            {
                Vocabulary_entry entry = entries.GetEnumerator().Current;
                return entry != null;
            }
            return false;
        }
        private bool CheckDictionaryConformityExists(Dictionary_conformity dictionaryConformity)
        {
            IEnumerable<Dictionary_conformity> conformities = unitOfWork.DictionaryConformityRepository.Get(x => (x.Number_string == dictionaryConformity.Number_string && x.ID_Vocabulary_entry == dictionaryConformity.ID_Vocabulary_entry));
            if (conformities != null)
            {
                Dictionary_conformity conformity = conformities.GetEnumerator().Current;
            }
            return false;
        }
        private void ProcessDictionaryTableRow(int rowNumber, DataTable data, SortedList<int, Vocabulary_entry> entries)
        {
            int curEntryNumber = tryParseEntryNumberDictionaryCell(rowNumber, data);
            if (curEntryNumber == 0)
            {
                // invalid value
                return;
            }

            Vocabulary_entry vocabularyEntry = null;
            if (!entries.ContainsKey(curEntryNumber))
            {
                vocabularyEntry = new Vocabulary_entry();
                vocabularyEntry.Number_vocabulary_entry = curEntryNumber;
                entries.Add(curEntryNumber, vocabularyEntry);
            }
            else
            {
                vocabularyEntry = entries[curEntryNumber];
            }

            // parse entry conponents in order of appearance
            if (vocabularyEntry != null)
            {
                // entry line number
                Dictionary_conformity dictionaryConformity = tryGetNewDictionaryConformity(rowNumber, data);
                if (dictionaryConformity == null)
                {
                    return;
                }

                // lemma - may be absent, it appears one time for each vocabilary entry
                tryParseAndSetLemmaDictionaryCell(rowNumber, vocabularyEntry, data);

                // language
                Language language = tryParseAndSetLanguageDictionaryCell(rowNumber, dictionaryConformity, data);
                if (language == null)
                {
                    return;
                }

                string reconstructedForm = tryParseAndSetReconstructedFormDictionaryCell(rowNumber, dictionaryConformity, data);
                if (String.IsNullOrEmpty(reconstructedForm))
                {
                    return;
                }

                tryParseAndSetVariantDictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetZeroPositionDictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition1DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition2DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition3DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition4DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition5DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition6DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition7DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition8DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition9DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition10DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition11DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetPosition_0DictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetReconstructedMeaningDictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetCommentDictionaryCell(rowNumber, dictionaryConformity, data);
                tryParseAndSetLinkDictionaryCell(rowNumber, dictionaryConformity, data);

                vocabularyEntry.Dictionary_conformity.Add(dictionaryConformity);
            }
        }
        private int tryParseEntryNumberDictionaryCell(int rowNumber, DataTable data)
        {
            int entryNumber = 0;
            int columnNumber = ExcelDictionaryColumn.ENTRY_NUMBER.Number;
            bool parsed = parseIntValue(rowNumber, columnNumber, data, out entryNumber);
            if (!parsed)
            {
                return 0;
            }
            return entryNumber;
        }
        private Dictionary_conformity tryGetNewDictionaryConformity(int rowNumber, DataTable data)
        {
            Dictionary_conformity conformity = null;
            int lineNumber = 0;
            int columnNumber = ExcelDictionaryColumn.LINE_NUMBER.Number;
            bool parsed = parseIntValue(rowNumber, columnNumber, data, out lineNumber);
            if (parsed)
            {
                conformity = new Dictionary_conformity();
                conformity.Number_string = lineNumber;
            }
            return conformity;
        }
        private string tryParseAndSetLemmaDictionaryCell(int rowNumber, Vocabulary_entry vocabularyEntry, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.LEMMA.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            vocabularyEntry.Lemma = cellValue;
            return cellValue;
        }
        private Language tryParseAndSetLanguageDictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            Language suitable = null;
            int columnNumber = ExcelDictionaryColumn.LANGUAGE.Number;
            string cellValue = data.Rows[rowNumber].Field<string>(columnNumber);
            if (!String.IsNullOrEmpty(cellValue))
            {
                if (possibleLanguages != null)
                {
                    suitable = getSuitableLanguage(cellValue);
                    if (suitable != null)
                    {
                        dictionaryConformity.Language = suitable;
                        dictionaryConformity.ID_Language = suitable.ID_Language;
                    }
                }
            }

            return suitable;
        }
        private Language getSuitableLanguage(string value)
        {
            Language suitable = null;
            if (!String.IsNullOrEmpty(value))
            {
                LanguagesEnum languageEnum = LanguagesEnum.parseSignature(value);
                if (languageEnum != null)
                {
                    string signature = languageEnum.Signature;
                    foreach (Language current in possibleLanguages)
                    {
                        if (signature.Equals(current.Sign))
                        {
                            suitable = current;
                        }
                    }
                }
            }
            return suitable;
        }
        private string tryParseAndSetReconstructedFormDictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.RECONSTRUCTED_FORM.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.Phorma = cellValue;
            return cellValue;
        }
        private string tryParseAndSetVariantDictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.VARIANT.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.Variant = cellValue;
            return cellValue;
        }
        private string tryParseAndSetZeroPositionDictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.ZERO_POSITION.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_00 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition1DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_1.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_1 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition2DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_2.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_2 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition3DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_3.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_3 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition4DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_4.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_4 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition5DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_5.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_5 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition6DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_6.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_6 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition7DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_7.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_7 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition8DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_8.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_8 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition9DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_9.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_9 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition10DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_10.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_10 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition11DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_11.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_11 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetPosition_0DictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.POSITION_0.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.p_0 = cellValue;
            return cellValue;
        }
        private string tryParseAndSetReconstructedMeaningDictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.RECONSTRUCTED_MEANING.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.Meaning = cellValue;
            return cellValue;
        }
        private string tryParseAndSetCommentDictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.COMMENT.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.Comment = cellValue;
            return cellValue;
        }
        private string tryParseAndSetLinkDictionaryCell(int rowNumber, Dictionary_conformity dictionaryConformity, DataTable data)
        {
            int columnNumber = ExcelDictionaryColumn.LINK.Number;
            string cellValue = parseStringValue(rowNumber, columnNumber, data);
            if (String.IsNullOrEmpty(cellValue))
            {
                return null;
            }
            dictionaryConformity.Link = cellValue;
            return cellValue;
        }
        private string parseStringValue(int rowNumber, int columnNumber, DataTable data)
        {
            string result = null;
            if (checkBoundaries(rowNumber, columnNumber, data))
            {
                string cellValue = data.Rows[rowNumber].Field<string>(columnNumber);
                if (!String.IsNullOrEmpty(cellValue))
                {
                    result = cellValue;
                }
            }
            return result;
        }
        private bool parseIntValue(int rowNumber, int columnNumber, DataTable data, out int value)
        {
            value = 0;
            if (checkBoundaries(rowNumber, columnNumber, data))
            {
                string cellValue = data.Rows[rowNumber].Field<string>(columnNumber);
                if (!String.IsNullOrEmpty(cellValue))
                {
                    bool parsed = Int32.TryParse(cellValue, out value);
                    return parsed;
                }
            }
            return false;
        }
        private bool checkBoundaries(int rowNumber, int columnNumber, DataTable data)
        {
            int columnsCount = data.Columns.Count;
            int rowsCount = data.Rows.Count;
            return columnNumber < columnsCount && rowNumber < rowsCount;
        }
    }
}