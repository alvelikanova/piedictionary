﻿using System.Collections.Generic;
using PIE_Dictionary.repositories;
using PIE_Dictionary.data;

namespace PIE_Dictionary.services
{
    public class PIEDataService
    {
        private static UnitOfWork unitOfWork;
        public PIEDataService()
        {
            unitOfWork = new UnitOfWork();
        }

        public IEnumerable<Vocabulary_entry> LoadVocabularyEntries()
        {
            return unitOfWork.VocabularyEntryRepository.Get();
        }

        public IEnumerable<Vocabulary_entry> LoadVocabularyEntriesWithLemma(string lemma)
        {
            return unitOfWork.VocabularyEntryRepository.Get(x => x.Lemma == lemma);
        }

        public IEnumerable<Dictionary_conformity> LoadDictionaryConformities(int entryNumber)
        {
            return unitOfWork.DictionaryConformityRepository.Get(x => x.Vocabulary_entry.Number_vocabulary_entry == entryNumber);
        }

        public IEnumerable<Language> LoadLanguages()
        {
            return unitOfWork.LanguageRepository.Get();
        }

        public void saveVocabularyEntry(Vocabulary_entry entry)
        {
            unitOfWork.VocabularyEntryRepository.Insert(entry);
        }

        public void saveDictionaryConformity(Dictionary_conformity conformity)
        {
            unitOfWork.DictionaryConformityRepository.Insert(conformity);
        }

        public void saveDictionaryConformities(IEnumerable<Dictionary_conformity> conformities)
        {
            if (conformities != null)
            {
                foreach (Dictionary_conformity conformity in conformities)
                {
                    saveDictionaryConformity(conformity);
                }
            }
        }

        public void deleteVocabularyEntry(Vocabulary_entry entry)
        {
            unitOfWork.VocabularyEntryRepository.Delete(entry);
        }

        public void deleteVocabularyEntities(IEnumerable<Vocabulary_entry> entries)
        {
            if (entries != null)
            {
                foreach (Vocabulary_entry entry in entries)
                {
                    deleteVocabularyEntry(entry);
                }
            }
        }

        public void deleteDictionaryConformity(Dictionary_conformity conformity)
        {
            unitOfWork.DictionaryConformityRepository.Delete(conformity);
        }

        public void deleteDictionaryConformities(IEnumerable<Dictionary_conformity> conformities)
        {
            if (conformities != null)
            {
                foreach (Dictionary_conformity conformity in conformities)
                {
                    deleteDictionaryConformity(conformity);
                }
            }
        }

        public IEnumerable<Phonetic_matching> getPhoneticMatchings(string phoneme, string language)
        {
            return unitOfWork.PhoneticMatchingRepository.Get(x => x.Phoneme1.Equals(phoneme) && x.Language.Sign.Equals(language));
        }

        public IEnumerable<Phonetic_matching> getPhoneticMatchings(string phoneme, string piePhoneme, string language)
        {
            return unitOfWork.PhoneticMatchingRepository.Get(x => x.Phoneme1.Equals(phoneme) && x.Phoneme.Equals(piePhoneme) && x.Language.Sign.Equals(language));
        }
    }
}