﻿using PIE_Dictionary.ui.model;
using PIE_Dictionary.data;
using PIE_Dictionary.services;
using PIE_Dictionary.commons;
using System.Collections.Generic;

namespace PIE_Dictionary.utils
{
    class PIEUtils
    {
        private PIEDataService pieDataService;
        public PIEUtils()
        {
            pieDataService = new PIEDataService();
        }
        public SplitMergeLogger DictionarySplitMerge()
        {
            SplitMergeLogger logger = new SplitMergeLogger();
            IEnumerable<Vocabulary_entry> entries = pieDataService.LoadVocabularyEntries();
            IEnumerable<VocabularyEntry> vocabularyEntries = ObjectMapper.mapVocabularyEntries(entries);
            if (vocabularyEntries != null)
            {
                DictionarySplit(vocabularyEntries, logger);
                DictionaryMerge(vocabularyEntries, logger);
            }
            return logger;
        }

        private void DictionarySplit(IEnumerable<VocabularyEntry> vocabularyEntries, SplitMergeLogger logger)
        {
            foreach (VocabularyEntry vocabularyEntry in vocabularyEntries)
            {
                IEnumerable<DictionaryConformity> conformities = getDictionaryConformities(vocabularyEntry);
                DictionaryConformity pieConformity = getPIEDictionaryConformity(conformities);
                if (pieConformity == null)
                {
                    vocabularyEntry.Regular = false;
                }
                else
                {
                    foreach (DictionaryConformity conformity in conformities)
                    {
                        if (isPIEConformity(conformity))
                        {
                            continue;
                        }
                        if (CheckPhoneticMismatch(conformity, pieConformity))
                        {
                            conformity.Regular = false;
                            vocabularyEntry.Regular = false;
                        }
                    }
                }
            }
        }

        private void DictionaryMerge(IEnumerable<VocabularyEntry> vocabularyEntries, SplitMergeLogger logger)
        {
            foreach (VocabularyEntry vocabularyEntry in vocabularyEntries)
            {
                if (vocabularyEntry.Regular)
                {
                    IEnumerable<Vocabulary_entry> entries = pieDataService.LoadVocabularyEntriesWithLemma(vocabularyEntry.Lemma);
                    mergeVocabularyEntries(entries, vocabularyEntry.Lemma, vocabularyEntry.Number);
                }
                else
                {
                    IEnumerable<DictionaryConformity> conformities = getDictionaryConformities(vocabularyEntry);
                    foreach (DictionaryConformity conformity in conformities)
                    {
                        List<string> reconstructedForms = buildReconstructedForms(conformity);
                        foreach (string reconstructedForm in reconstructedForms)
                        {
                            IEnumerable<Vocabulary_entry> entries = pieDataService.LoadVocabularyEntriesWithLemma(reconstructedForm);
                            mergeVocabularyEntries(entries, reconstructedForm, vocabularyEntry.Number);
                        }
                    }
                }
            }
        }

        private bool isPIEConformity(DictionaryConformity conformity)
        {
            return "ПИЕ*".Equals(conformity.Language);
        }
        private DictionaryConformity getPIEDictionaryConformity(IEnumerable<DictionaryConformity> conformities)
        {
            if (conformities != null)
            {
                foreach (DictionaryConformity conformity in conformities)
                {
                    if (isPIEConformity(conformity))
                    {
                        return conformity;
                    }
                }
            }
            return null;
        }
        private bool CheckPhoneticMismatch(DictionaryConformity dictionaryConformity, DictionaryConformity pieDictionaryConformity)
        {
            PhoneticMatchingType type = CheckPhoneticMatchings(dictionaryConformity, pieDictionaryConformity);
            if (type != null)
            {
                if (!type.isRegular())
                {
                    PhoneticMatchingType commentType = CheckCommentExists(dictionaryConformity);
                    return commentType == type;
                }
            }
            return false;
        }

        private PhoneticMatchingType CheckPhoneticMatchings(DictionaryConformity dictionaryConformity, DictionaryConformity pieDictionaryConformity)
        {
            string languageSignature = dictionaryConformity.Language;
            PhoneticMatchingType match = CheckPhoneticMatching(dictionaryConformity.P_1, pieDictionaryConformity.P_1, languageSignature);
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_2, pieDictionaryConformity.P_2, languageSignature);
            }
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_3, pieDictionaryConformity.P_3, languageSignature);
            }
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_4, pieDictionaryConformity.P_4, languageSignature);
            }
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_5, pieDictionaryConformity.P_5, languageSignature);
            }
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_6, pieDictionaryConformity.P_6, languageSignature);
            }
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_7, pieDictionaryConformity.P_7, languageSignature);
            }
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_8, pieDictionaryConformity.P_8, languageSignature);
            }
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_9, pieDictionaryConformity.P_9, languageSignature);
            }
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_10, pieDictionaryConformity.P_10, languageSignature);
            }
            if (match.isRegular())
            {
                match = CheckPhoneticMatching(dictionaryConformity.P_11, pieDictionaryConformity.P_11, languageSignature);
            }
            return match;
        }

        private PhoneticMatchingType CheckPhoneticMatching(string phoneme, string piePhoneme, string language)
        {
            IEnumerable<Phonetic_matching> matchings = pieDataService.getPhoneticMatchings(phoneme, piePhoneme, language);
            if (matchings != null && matchings.GetEnumerator().Current != null)
            {
                return PhoneticMatchingType.REGULAR;
            }
            return PhoneticMatchingType.IRREGULAR;
        }
        private PhoneticMatchingType CheckCommentExists(DictionaryConformity dictionaryConformity)
        {
            string comment = dictionaryConformity.Comment;
            if (comment.Equals("метатеза"))
            {
                return PhoneticMatchingType.METATHESIS;
            }
            return PhoneticMatchingType.REGULAR;
        }

        private VocabularyEntry createNewVocabilaryEntry(DictionaryConformity dictionaryConformity, bool regular)
        {
            VocabularyEntry entry = new VocabularyEntry();
            entry.DictionaryConformities.Add(dictionaryConformity);
            entry.AutoGenerated = true;
            entry.Regular = regular;
            return entry;
        }
        private List<string> buildReconstructedForms(DictionaryConformity conformity)
        {
            if (conformity == null)
            {
                return null;
            }
            string lemma = conformity.Phorma;
            string language = conformity.Language;
            if (lemma == null)
            {
                return null;
            }
            int positionsCount = lemma.Length;
            if (positionsCount <= 0)
            {
                return null;
            }

            Dictionary<int, List<string>> matchings = new Dictionary<int, List<string>>();
            for (int i = 0; i < positionsCount; i++)
            {
                string position = lemma[i].ToString();
                matchings.Add(i, getPhonemeMatchings(position, language));
            }

            return generateAllPossibleForms(matchings);
        }
        private List<string> getPhonemeMatchings(string phoneme, string languageSignature)
        {
            List<string> result = new List<string>();
            IEnumerable<Phonetic_matching> matchings = pieDataService.getPhoneticMatchings(phoneme, languageSignature);
            if (matchings != null)
            {
                foreach (Phonetic_matching matching in matchings)
                {
                    result.Add(System.Uri.UnescapeDataString(matching.Phoneme1.Unicode));
                }
            }
            return result;
        }

        private List<string> generateAllPossibleForms(Dictionary<int, List<string>> matchings)
        {
            List<string> result = new List<string>();
            int positionsCount = matchings.Keys.Count;
            for (int i = positionsCount - 1; i > 0; i++)
            {
                List<string> phonemes = matchings[i];
                concatPhonemes(phonemes, result);
            }
            return result;
        }

        private void concatPhonemes(List<string> phonemes, List<string> tails)
        {
            if (tails.Count == 0)
            {
                initializeTails(phonemes, tails);
            }
            else
            {
                List<string> result = new List<string>();
                int phonemesCount = phonemes.Count;
                for (int i = 0; i < phonemesCount; i++)
                {
                    List<string> tailsCopy = copyTails(tails);
                    appendPhoneme(phonemes[i], tailsCopy);
                    result.AddRange(tailsCopy);
                }
                tails.Clear();
                tails.AddRange(result);
            }
        }

        private void initializeTails(ICollection<string> phonemes, ICollection<string> tails)
        {
            tails.Clear();
            foreach (string phoneme in phonemes)
            {
                tails.Add(phoneme);
            }
        }

        private void appendPhoneme(string phoneme, List<string> tails)
        {
            for (int i = 0; i < tails.Count; i++)
            {
                string tail = tails[i];
                tail += phoneme;
            }
        }

        private List<string> copyTails(List<string> tails)
        {
            List<string> copy = new List<string>();
            copy.AddRange(tails);
            return copy;
        }

        private IEnumerable<DictionaryConformity> getDictionaryConformities(VocabularyEntry vocabularyEntry)
        {
            IEnumerable<Dictionary_conformity> conformities = pieDataService.LoadDictionaryConformities(vocabularyEntry.Number);
            return ObjectMapper.mapDictionaryConformities(conformities);
        }

        private IEnumerable<VocabularyEntry> getVocabularyEntriesWithLemma(string lemma)
        {
            IEnumerable<Vocabulary_entry> entries = pieDataService.LoadVocabularyEntriesWithLemma(lemma);
            return ObjectMapper.mapVocabularyEntries(entries);
        }

        private void mergeVocabularyEntries(IEnumerable<Vocabulary_entry> entries, string lemma, int number)
        {
            if (entries != null)
            {
                Vocabulary_entry result = new Vocabulary_entry();
                result.Lemma = lemma;
                result.Number_vocabulary_entry = number;
                result.Dictionary_conformity = new List<Dictionary_conformity>();
                foreach (Vocabulary_entry entry in entries)
                {
                    IEnumerable<Dictionary_conformity> conformities = entry.Dictionary_conformity;
                    foreach (Dictionary_conformity conformity in conformities)
                    {
                        if (!checkConformityExistsInVocabularyEntry(conformity, entry))
                        {
                            Dictionary_conformity copy = copyDictionaryConformity(conformity, entry);
                            result.Dictionary_conformity.Add(copy);
                        }
                    }
                    pieDataService.deleteDictionaryConformities(conformities);
                }

                pieDataService.deleteVocabularyEntities(entries);
                pieDataService.saveVocabularyEntry(result);
                pieDataService.saveDictionaryConformities(result.Dictionary_conformity);
            }
        }

        private bool checkConformityExistsInVocabularyEntry(Dictionary_conformity conformity, Vocabulary_entry entry)
        {
            if (conformity != null && entry != null)
            {
                string languageSignature = conformity.Language.Sign;
                IEnumerable<Dictionary_conformity> entryConformities = entry.Dictionary_conformity;
                foreach (Dictionary_conformity entryConformity in entryConformities)
                {
                    if (languageSignature.Equals(entryConformity.Language.Sign))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private Dictionary_conformity copyDictionaryConformity(Dictionary_conformity conformity, Vocabulary_entry vocabularyEntry)
        {
            if (conformity != null)
            {
                return new Dictionary_conformity
                {
                    Comment = conformity.Comment,
                    ID_Language = conformity.ID_Language,
                    Language = conformity.Language,
                    Link = conformity.Link,
                    Meaning = conformity.Meaning,
                    Number_string = conformity.Number_string,
                    Phorma = conformity.Phorma,
                    p_0 = conformity.p_0,
                    p_00 = conformity.p_00,
                    p_1 = conformity.p_1,
                    p_10 = conformity.p_10,
                    p_11 = conformity.p_11,
                    p_2 = conformity.p_2,
                    p_3 = conformity.p_3,
                    p_4 = conformity.p_4,
                    p_5 = conformity.p_5,
                    p_6 = conformity.p_6,
                    p_7 = conformity.p_7,
                    p_8 = conformity.p_8,
                    p_9 = conformity.p_9,
                    Variant = conformity.Variant,
                    Vocabulary_entry = vocabularyEntry
                };
            }
            else
            {
                return null;
            }
        }
    }
}