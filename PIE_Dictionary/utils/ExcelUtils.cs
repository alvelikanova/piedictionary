﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace PIE_Dictionary.utils
{
    class ExcelUtils
    {
        public static DataTable LoadDataTableFromExcel(string documentPath)
        {
            if (!File.Exists(documentPath))
            {
                throw new ArgumentException("Document path does not exist");
            }
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + documentPath + @";Extended Properties=""Excel 12.0;HDR=No;IMEX=1""";
            DataTable table = null;
            using (var conn = new OleDbConnection(connString))
            {
                conn.Open();
                var dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                var sheet1 = dtSchema.Rows[0].Field<string>("TABLE_NAME");
                string query = "SELECT  * FROM [" + sheet1 + "]";
                var adapter = new OleDbDataAdapter(query, conn);
                table = new DataTable();
                adapter.Fill(table);
            }
            return table;
        }
    }
}
