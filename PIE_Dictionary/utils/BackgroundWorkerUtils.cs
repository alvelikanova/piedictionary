﻿using System;
using System.ComponentModel;
using System.Threading;

namespace PIE_Dictionary.utils
{
    class BackgroundWorkerUtils
    {
        public static int GetProgress(int total, int processed)
        {
            return Convert.ToInt32(Math.Round((double)(100 * processed) / total));
        }
        public static void ReportProgress(object sender, int progress, object eventArgs)
        {
            if (sender != null)
            {
                if (sender is BackgroundWorker)
                {
                    BackgroundWorker worker = (sender as BackgroundWorker);
                    worker.ReportProgress(progress, eventArgs);
                    Thread.Sleep(1);
                }
            }
        }
    }
}
