﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_Dictionary.utils
{
    class IOUtils
    {
        public static bool CheckFilepathIsValid(string filepath)
        {
            return !String.IsNullOrEmpty(filepath) && File.Exists(filepath);
        }
    }
}
