﻿using PIE_Dictionary.data;
using PIE_Dictionary.ui.model;
using System.Collections.Generic;

namespace PIE_Dictionary.utils
{
    public class ObjectMapper
    {
        public static Dictionary_conformity mapDictionaryConformity(DictionaryConformity dto)
        {
            if (dto != null)
            {

                int number = dto.Number;
                Language language = null;
                string LanguageSignature = dto.Language;
                if (LanguageSignature != null)
                {
                    // TODO load from DB
                }

                string meaning = dto.Meaning;
                string phorma = dto.Phorma;
                string p_00 = dto.P_00;
                string p_1 = dto.P_1;
                string p_2 = dto.P_2;
                string p_3 = dto.P_3;
                string p_4 = dto.P_4;
                string p_5 = dto.P_5;
                string p_6 = dto.P_6;
                string p_7 = dto.P_7;
                string p_8 = dto.P_8;
                string p_9 = dto.P_9;
                string p_10 = dto.P_10;
                string p_11 = dto.P_11;
                string p_0 = dto.P_0;
                string comment = dto.Comment;

                // TODO load missing columns
                return new Dictionary_conformity { Comment = comment, Phorma = phorma, ID_Dictionary_conformity = 0, ID_Language = language.ID_Language, ID_Vocabulary_entry = null, Language = language, Link = null, Meaning = null, Number_string = null, p_0 = p_0, p_00 = p_00, p_1 = p_1, p_10 = p_10, p_11 = p_11, p_2 = p_2, p_3 = p_3, p_4 = p_4, p_5 = p_5, p_6 = p_6, p_7 = p_7, p_8 = p_8, p_9 = p_9, Variant = null, Vocabulary_entry = null };
            }
            else
            {
                return null;
            }
        }
        public static DictionaryConformity mapDictionaryConformity(Dictionary_conformity entity)
        {
            if (entity != null)
            {
                int number = entity.Number_string.Value;
                string languageSignature = null;
                Language language = entity.Language;
                if (language != null)
                {
                    languageSignature = language.Sign;
                }
                string meaning = entity.Meaning;
                string phorma = entity.Phorma;
                string p_00 = entity.p_00;
                string p_1 = entity.p_1;
                string p_2 = entity.p_2;
                string p_3 = entity.p_3;
                string p_4 = entity.p_4;
                string p_5 = entity.p_5;
                string p_6 = entity.p_6;
                string p_7 = entity.p_7;
                string p_8 = entity.p_8;
                string p_9 = entity.p_9;
                string p_10 = entity.p_10;
                string p_11 = entity.p_11;
                string p_0 = entity.p_0;
                string comment = entity.Comment;

                return new DictionaryConformity { Comment = comment, Language = languageSignature, Meaning = meaning, Number = number, Phorma = phorma, P_0 = p_0, P_00 = p_00, P_1 = p_1, P_10 = p_10, P_11 = p_11, P_2 = p_2, P_3 = p_3, P_4 = p_4, P_5 = p_5, P_6 = p_6, P_7 = p_7, P_8 = p_8, P_9 = p_9 };
            }
            else
            {
                return null;
            }
        }

        public static IEnumerable<DictionaryConformity> mapDictionaryConformities(IEnumerable<Dictionary_conformity> entities)
        {
            List<DictionaryConformity> result = new List<DictionaryConformity>();
            if (entities != null)
            {
                foreach (Dictionary_conformity conformity in entities)
                {
                    result.Add(mapDictionaryConformity(conformity));
                }
            }
            return result;
        }

        public static VocabularyEntry mapVocabularyEntry(Vocabulary_entry entity)
        {
            if (entity != null)
            {
                int number = entity.Number_vocabulary_entry.HasValue ? entity.Number_vocabulary_entry.Value : 0;
                return new VocabularyEntry { AutoGenerated = false, Lemma = entity.Lemma, Number = number, Regular = true };
            }
            else
            {
                return null;
            }
        }

        public static IEnumerable<VocabularyEntry> mapVocabularyEntries(IEnumerable<Vocabulary_entry> entities)
        {
            List<VocabularyEntry> result = new List<VocabularyEntry>();
            if (entities != null)
            {
                foreach (Vocabulary_entry entity in entities)
                {
                    result.Add(mapVocabularyEntry(entity));
                }
            }
            return result;
        }
    }
}