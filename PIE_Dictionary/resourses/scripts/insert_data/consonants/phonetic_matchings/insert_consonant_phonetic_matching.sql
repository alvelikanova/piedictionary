﻿INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ph' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_CONS_OR_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm' AND
[dbo].[Rule].[Code] = 'AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ell';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ch as x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BEFORE_PLOSIVE';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b as beta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'v' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'p' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/p/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/p/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/p/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/p/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/p/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/p/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/p/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sp' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sp' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sp' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'th' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'theta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_CONS_OR_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't as t^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z as ts' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as c' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'th as theta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'thorn as theta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd as eth' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'AFTER_STOP_OR_S';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'th' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'AFTER_STOP_OR_S';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'tt' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'tth' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's/theta/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_CONS_OR_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, 1
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'zt as tst' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, 1
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ss' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ss' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ss' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ss' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ss' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/t/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/t/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/t/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/t/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sht as /esh/t' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/t/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/t/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/t/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/t/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'st' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'st' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with acute as s with curl' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with caron as esh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'th as theta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 't+t' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ch as x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ch as x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ch as x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'y' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'y' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as t/esh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as t/esh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'kh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'kh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as t/esh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as t/esh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_CONS_OR_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_CONS_OR_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c with caron as t/esh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c with caron as t/esh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as ts' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as ts' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'q as c' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'k' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k as k^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k as k^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ku' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BEFORE_OR_AFTER_U';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'qu as k^w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'BEFORE_OR_AFTER_O_U';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'hwair as turned w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gw' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'wh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = '/c with acute/h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = '/c with acute//c with acute/h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, 1
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, 1
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with caron' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, 2
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c with caron' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm' AND
[dbo].[Rule].[Code] = 'AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k with acute/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k with acute/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k^w/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k^w/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k^w/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k^w/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k^w/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k^w/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k^w/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'normal development of /s/+/k^w/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'kh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'AFTER_R_L_M_N_T_D';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'kh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'AFTER_R_L_M_N_T_D';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'skh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'AFTER_GREEK_TH';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'skh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'AFTER_GREEK_TH';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sc as sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sc as sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sc as sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sc as sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sh as esh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'sk' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sh as esh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sh as esh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'squ as s/k^w/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sc as sk' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's/k^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sq' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'bh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'beta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'YG_AVESTAN_AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b as b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b as beta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'b' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'dh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'delta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'YG_AVESTAN_AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'dh as eth' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ts' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with acute as s with curl' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'd' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd as eth' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as d/ezh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as h with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z with caron as ezh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'dh as eth' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as ts' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ch' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ch' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as d/ezh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as h with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as h with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as d/ezh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as h with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_ORIGINAL_LARYNGEAL';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as h with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as d/ezh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'YG_AVESTAN_AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as d/ezh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'YG_AVESTAN_AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z with caron as ezh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'dz' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z with caron as ezh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'dz' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gj as dotless j with stroke' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ku' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g with acute/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with acute as s with curl' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'g' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with acute as s with curl' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with acute as s with curl' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BEFORE_OR_AFTER_U';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'u [w > v]' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gu as g^w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'AFTER_N';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'q as k^w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'qu' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b as b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^w/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b as beta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b as b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b as beta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'bh as b^/h with hook/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'beta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'YG_AVESTAN_AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'p' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ph as p^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'WB';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b as b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b as beta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'EW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'v' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS_AND_R_L';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/b^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS_AND_R_L';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'dh as d^/h with hook/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'delta' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'YG_AVESTAN_AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 't' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'c as c' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'th as t^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'WB';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'AFTER_U_R_OR_BEFORE_R_L';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd as d' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd as eth' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd as eth' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'thorn' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'EW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/d^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as h with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z with caron as ezh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'dh as eth' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'd' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as dz' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'k' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with acute as s with curl' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with acute as s with curl' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with acute as s with curl' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ch as k^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ch as k^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'BEFORE_SONORANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'EW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g as x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'EW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'y' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS_AND_R_L';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'y' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS_AND_R_L';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '//g with acute/^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS_AND_R_L';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS_AND_R_L';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gh as g^/h with hook/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gh as g^/h with hook/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as h with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as h with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as d/ezh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as d/ezh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'YG_AVESTAN_AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gamma' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'YG_AVESTAN_AFTER_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z with caron as ezh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z with caron as ezh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'dz' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'dz' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gj as dotless j with stroke' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_SECONDARY_FRONT_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^h/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j with caron as d/ezh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j with caron as d/ezh/' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ku' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ph as p^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'th as t^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BEFORE_FRONT_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ch as k^h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BEFORE_OR_AFTER_U';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'WB';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'u as w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gu as g^w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'AFTER_N';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'WB';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gw' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'AFTER_N';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'b' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'WB';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/g^(wh)/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with dot below as s with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr' AND
[dbo].[Rule].[Code] = 'RUKI_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h as x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'BEFORE_PLOSIVE';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with caron as esh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave' AND
[dbo].[Rule].[Code] = 'RUKI_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'x as x' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'RUKI_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with caron as esh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit' AND
[dbo].[Rule].[Code] = 'RUKI_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'sh as esh' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gj as dotless j with stroke' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BEFORE_STRESSED_VOW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm' AND
[dbo].[Rule].[Code] = 'BEFORE_PLOSIVE';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with caron as s' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's with dot below as s with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'WB';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BEFORE_OR_AFTER_OBSTRUENT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'EW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'macron' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BEFORE_OR_AFTER_RESONANT';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's as s' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's as h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 's' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 's' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng' AND
[dbo].[Rule].[Code] = 'VERNER_LAW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm as m' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'm' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm as w with tilde' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ogonek as tilde' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'm' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '-m' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'ogonek as tilde' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS' AND
[dbo].[Rule].[Code] = 'EW';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'n' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'n with tilde as n with left hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, 'dial. l', null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'll as l with middle tilde' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'l' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'l with middle tilde as [l with middle tilde > gamma]' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'r as r with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = 'r' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'rr as r' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'y as j' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'y as j' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as j' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j as j' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'gj as dotless j with stroke' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'y as j' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'y as j' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'z [zd > dz > z]' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'h' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Greek' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'i as j' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'j' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/i with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'y' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Got';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Eng';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'f' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language], [dbo].[Rule]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'N/A' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Sga' AND
[dbo].[Rule].[Code] = 'BTW_VOWS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Hit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Toch A';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'v' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'OCS';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'v' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Alb';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'v as v with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Skr';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'v as w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Ave';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'v as v with hook' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lit';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'g' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'w' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Arm';

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM [dbo].[Phoneme] AS [PIE_Phoneme], [dbo].[Phoneme] AS [Language_Phoneme], [dbo].[Language]
WHERE [PIE_Phoneme].[Record] = '/u with breve/' AND [PIE_Phoneme].[Reconsr] = 1 AND
[Language_Phoneme].[Record] = 'u [w > v]' AND [Language_Phoneme].[Reconsr] = 0 AND 
[dbo].[Language].[Sign] = 'Lat';