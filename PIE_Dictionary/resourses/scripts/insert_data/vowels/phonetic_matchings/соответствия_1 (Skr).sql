-----------------------------------------------------------------------------------------------------------------------------------------
--Skr
INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'e' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'a' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'o' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = 'o' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr' 
	AND [dbo].[Rule].[Code] = 'BRUGMANN_LAW'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/schwa/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '-' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/O with stroke/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/a with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/i with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'u' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/u with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'eu' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ou' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'au' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/u' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/u' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/m' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'am' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below/n' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'an' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/r with dot below/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with macron/r' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with macron/r' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr' 
	AND [dbo].[Rule].[Code] = 'NBH_LABIALS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/l' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ir' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/l' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ur' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr' 
	AND [dbo].[Rule].[Code] = 'NBH_LABIALS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/r with dot below/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with macron/r' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with macron/r' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr' 
	AND [dbo].[Rule].[Code] = 'NBH_LABIALS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/r' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ir' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/r' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ur' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr' 
	AND [dbo].[Rule].[Code] = 'NBH_LABIALS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ei' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ai > /e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'oi' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ai > /e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ai' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ai > /e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/a with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr' 
	AND [dbo].[Rule].[Code] = 'FINAL_SYL'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr' 
	AND [dbo].[Rule].[Code] = 'FINAL_SYL'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/a with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Skr' 
	AND [dbo].[Rule].[Code] = 'FINAL_SYL'
;