--Greek
INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'e' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'e' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'a' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'o' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'o' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/schwa/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'e' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/schwa/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/schwa/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'o' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '-' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '-' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'o' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/i with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ei' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ei' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'oi' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'oi' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ai' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ai' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/e with macron/i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/o with macron/i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'u' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/u with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'eu' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'eu' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ou' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ou' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'au' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'au' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'm/e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'm/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/m' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'am' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'n/e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'n/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below/n' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'an' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'la' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'l/e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'l/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/l' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'al' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ra' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'r/e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'r/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/r' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ar' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'l/a with macron/ > l/e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek' 
	AND [dbo].[Rule].[Code] = 'A_GT_E'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'n/a with macron/ > n/e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek' 
	AND [dbo].[Rule].[Code] = 'A_GT_E'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'm/a with macron/ > m/e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek' 
	AND [dbo].[Rule].[Code] = 'A_GT_E'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/a with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/i > /e with macron/i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek' 
	AND [dbo].[Rule].[Code] = 'A_GT_E'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/a with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/ > /e with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek' 
	AND [dbo].[Rule].[Code] = 'A_GT_E'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], '?', null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/u with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with macron/ or (w)/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek' 
	AND [dbo].[Rule].[Code] = 'DISPUTED_BREAKING'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], '?', null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/u with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with macron/ or (w)/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek' 
	AND [dbo].[Rule].[Code] = 'DISPUTED_BREAKING'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], '?', null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/i with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with macron/ or (j)/o with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek' 
	AND [dbo].[Rule].[Code] = 'DISPUTED_BREAKING'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], '?', null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/i with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with macron/ or (j)/a with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek' 
	AND [dbo].[Rule].[Code] = 'DISPUTED_BREAKING'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '-' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'e (o)' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Greek'
;