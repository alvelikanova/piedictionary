--Laryng. PIE
INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'e' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'e' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'e' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h1e' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = 'a' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'DISPUTED_NON_ALLOPHONIC_A'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'a' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h2e' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'o' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'o' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'o' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h3e' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/schwa/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h1' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'BTW_CONS_OR_AFTER_CONS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/schwa/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h2' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'BTW_CONS_OR_AFTER_CONS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/schwa/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h3' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'BTW_CONS_OR_AFTER_CONS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '-' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h1-' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'WB_CONS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '-' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h2-' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'WB_CONS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '-' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h3-' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'WB_CONS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/e with macron/' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'eh1' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/a with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/a with macron/' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'DISPUTED_NON_ALLOPHONIC_A'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/a with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'eh2' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/o with macron/' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'eh3' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/i with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ih1' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/i with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ih2' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/i with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ih3' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ei' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ei' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ei' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h1ei' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'oi' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'oi' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'oi' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h3ei' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = 'ai' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ai' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'DISPUTED_NON_ALLOPHONIC_A'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ai' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h2ei' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/e with macron/i' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, 'oei', null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/o with macron/i' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/a with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'eh2ei' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'u' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/u with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'uh1' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/u with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'uh2' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/u with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'uh3' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'eu' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'eu' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'eu' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h1eu' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ou' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ou' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ou' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h3eu' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = 'au' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'au' AND [Language_Phoneme].[Reconsr] = 1 
	AND [dbo].[Language].[Sign] = 'Laryng. PIE' 
	AND [dbo].[Rule].[Code] = 'DISPUTED_NON_ALLOPHONIC_A'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'au' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'h2eu' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/e with macron/u' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/o with macron/u' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/m with combining ring below/' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'mh1' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'mh2' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'mh3' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/m' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/m with combining ring below/m' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/n with combining ring below/' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'nh1' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'nh2' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'nh3' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below/n' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/n with combining ring below/n' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/l with combining ring below/' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'lh1' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'lh2' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'lh3' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/l' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/l with combining ring below/l' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/r with combining ring below/' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'rh1' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'rh2' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'rh3' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/r' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/r with combining ring below/r' AND [Language_Phoneme].[Reconsr] = 1
	AND [dbo].[Language].[Sign] = 'Laryng. PIE'
;