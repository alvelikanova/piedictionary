-----------------------------------------------------------------------------------------------------------------------------------------
--Lit
INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'e' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'e' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'a' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'o' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'a' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/schwa/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/O with stroke/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '-' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/O with stroke/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/e with dot above/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'uo' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ei' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ei' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = 'ei' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ie' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'UNDER_STRESS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'oi' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ai' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ai' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ai' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = 'oi' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ie' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'UNDER_STRESS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = 'ai' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ie' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'UNDER_STRESS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ai' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/o with macron/i' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ui' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'FINAL_SYL'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'u' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/u with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with macron/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'eu' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'iau' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'ou' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'au' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = 'au' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'au' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/e with macron/u' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'iau' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i/m with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'u/m with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with grave/m' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with grave/m' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/m' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'im' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/m with combining ring below/m' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'um' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i/n with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'u/n with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with grave/n' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with grave/n' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below/n' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i/n with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/n with combining ring below/n' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'u/n with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i/l with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'u/l with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with grave/l' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with grave/l' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/l' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'il' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/l with combining ring below/l' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ul' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'i/r with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'u/r with tilde/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/i with grave/r' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = '/u with grave/r' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/r' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ir' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], [dbo].[Rule].[ID_Rule], null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language], 
	[dbo].[Rule]
WHERE 
	[PIE_Phoneme].[Record] = '/r with combining ring below/r' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'ur' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit' 
	AND [dbo].[Rule].[Code] = 'NBH_VELARS'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/a with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'o as /oː/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;

INSERT INTO [dbo].[Phonetic_matching] ([ID_Phoneme_PIE], [ID_Phoneme_language], [ID_Language], [ID_Rule], [Comment], [Сonfidence_level]) 
SELECT [PIE_Phoneme].[ID_Phoneme], [Language_Phoneme].[ID_Phoneme], [dbo].[Language].[ID_Language], null, null, null
FROM 
	[dbo].[Phoneme] AS [PIE_Phoneme], 
	[dbo].[Phoneme] AS [Language_Phoneme], 
	[dbo].[Language]
WHERE 
	[PIE_Phoneme].[Record] = '/i with macron/' AND [PIE_Phoneme].[Reconsr] = 1 
	AND [Language_Phoneme].[Record] = 'y as /i:/' AND [Language_Phoneme].[Reconsr] = 0 
	AND [dbo].[Language].[Sign] = 'Lit'
;