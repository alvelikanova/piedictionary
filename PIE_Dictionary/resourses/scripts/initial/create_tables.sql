﻿CREATE TABLE [dbo].[Branch] (
    [ID_Branch]        INT            NOT NULL IDENTITY(1,1),
    [Name]             NVARCHAR (MAX) NULL,
    [Sign]             NVARCHAR (50)  NOT NULL,
    [ID_branch_parent] INT            NULL,
    CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED ([ID_Branch] ASC),
    CONSTRAINT [FK_Branch_Branch] FOREIGN KEY ([ID_branch_parent]) REFERENCES [dbo].[Branch] ([ID_Branch]),
	CONSTRAINT [UQ_Branch_Signature] UNIQUE ([Sign])
);
CREATE TABLE [dbo].[Language] (
    [ID_Language] INT            NOT NULL IDENTITY(1,1),
    [Name]        NVARCHAR (MAX) NULL,
    [Sign]        NVARCHAR (50)  NOT NULL,
    [ID_Branch]   INT            NOT NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([ID_Language] ASC),
    CONSTRAINT [FK_Language_Branch] FOREIGN KEY ([ID_Branch]) REFERENCES [dbo].[Branch] ([ID_Branch]) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT [UQ_Language_Signature] UNIQUE ([Sign])
);
CREATE TABLE [dbo].[Rule] (
    [ID_Rule]   INT                  NOT NULL IDENTITY(1,1),
    [Rule_desc] NVARCHAR (MAX)       NULL,
    [Code]      NVARCHAR (50)        NOT NULL,
	[Vowel_consonant] BIT            NOT NULL,
    CONSTRAINT [PK_Rule] PRIMARY KEY CLUSTERED ([ID_Rule] ASC),
	CONSTRAINT [UQ_Rule_Code] UNIQUE ([Code])
);
CREATE TABLE [dbo].[Phoneme] (
    [ID_Phoneme]      INT            NOT NULL IDENTITY(1,1),
    [Record]          NVARCHAR (100) NULL,
    [Transcrip]       NVARCHAR (120) NULL,
	[Unicode]         NVARCHAR (120) NULL,
    [Phoneme_desc]    NVARCHAR (MAX) NULL,
    [Vowel_consonant] BIT            NULL,
    [Reconsr]         BIT            NULL,
    CONSTRAINT [PK_Phoneme] PRIMARY KEY CLUSTERED ([ID_Phoneme] ASC),
	CONSTRAINT [UQ_Phoneme_Signature] UNIQUE ([Record], [Vowel_consonant], [Reconsr])
);
CREATE TABLE [dbo].[Vocabulary_entry] (
    [ID_Vocabulary_entry]     INT            NOT NULL IDENTITY(1,1),
    [Number_vocabulary_entry] INT            NULL,
    [Lemma]                   NVARCHAR (200) NULL,
    CONSTRAINT [PK_Vocabulary_entry] PRIMARY KEY CLUSTERED ([ID_Vocabulary_entry] ASC),
	CONSTRAINT [UQ_Vocabulary_Entry_Number] UNIQUE ([Number_vocabulary_entry])
);
CREATE TABLE [dbo].[Phonetic_matching] (
    [ID_Phonetic_matching] INT            NOT NULL IDENTITY(1,1),
    [ID_Phoneme_PIE]       INT            NULL,
    [ID_Phoneme_language]  INT            NULL,
    [ID_Language]          INT            NULL,
    [ID_Rule]              INT            NULL,
    [Comment]              NVARCHAR (MAX) NULL,
    [Сonfidence_level]     INT            NULL,
    CONSTRAINT [PK_Phonetic_matching] PRIMARY KEY CLUSTERED ([ID_Phonetic_matching] ASC),
    CONSTRAINT [FK_Phonetic_matching_Language] FOREIGN KEY ([ID_Language]) REFERENCES [dbo].[Language] ([ID_Language]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Phonetic_matching_Phoneme] FOREIGN KEY ([ID_Phoneme_PIE]) REFERENCES [dbo].[Phoneme] ([ID_Phoneme]),
    CONSTRAINT [FK_Phonetic_matching_Phoneme1] FOREIGN KEY ([ID_Phoneme_language]) REFERENCES [dbo].[Phoneme] ([ID_Phoneme]) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT [FK_Phonetic_matching_Rule] FOREIGN KEY ([ID_Rule]) REFERENCES [dbo].[Rule] ([ID_Rule]) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE [dbo].[Dictionary_conformity] (
    [ID_Dictionary_conformity] INT            NOT NULL IDENTITY(1,1),
    [ID_Vocabulary_entry]      INT            NULL,
    [Number_string]            INT            NULL,
    [ID_Language]              INT            NULL,
    [Phorma]                   NVARCHAR (200) NULL,
    [Variant]                  NVARCHAR (200) NULL,
    [p_00]                     NVARCHAR (50)  NULL,
    [p_1]                      NVARCHAR (50)  NULL,
    [p_2]                      NVARCHAR (50)  NULL,
    [p_3]                      NVARCHAR (50)  NULL,
    [p_4]                      NVARCHAR (50)  NULL,
    [p_5]                      NVARCHAR (50)  NULL,
    [p_6]                      NVARCHAR (50)  NULL,
    [p_7]                      NVARCHAR (50)  NULL,
    [p_8]                      NVARCHAR (50)  NULL,
    [p_9]                      NVARCHAR (50)  NULL,
    [p_10]                     NVARCHAR (50)  NULL,
    [p_11]                     NVARCHAR (50)  NULL,
    [p_0]                      NVARCHAR (50)  NULL,
    [Meaning]                  NVARCHAR (MAX) NULL,
    [Comment]                  NVARCHAR (MAX) NULL,
    [Link]                     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Dictionary_conformity] PRIMARY KEY CLUSTERED ([ID_Dictionary_conformity] ASC),
    CONSTRAINT [FK_Dictionary_conformity_Language] FOREIGN KEY ([ID_Language]) REFERENCES [dbo].[Language] ([ID_Language]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Dictionary_conformity_Vocabulary_entry] FOREIGN KEY ([ID_Vocabulary_entry]) REFERENCES [dbo].[Vocabulary_entry] ([ID_Vocabulary_entry]) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT [UQ_Entry_Number] UNIQUE ([ID_Vocabulary_entry], [Number_string])
);