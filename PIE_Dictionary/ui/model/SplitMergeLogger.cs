﻿using System.Collections.Generic;

namespace PIE_Dictionary.ui.model
{
    public class SplitMergeLogger
    {
        private List<SplitMergeLoggerEntry> entries = new List<SplitMergeLoggerEntry>();
        public void recordChange(int vocabularyNumber, Action action)
        {
            entries.Add(new SplitMergeLoggerEntry { Action = action, VocabularyNumber = vocabularyNumber });
        }
    }
    public enum Action
    {
        Merged, Deleted, Created, Splitted
    }
    class SplitMergeLoggerEntry
    {
        public int VocabularyNumber { get; set; }
        public Action Action { get; set; }
    }
}