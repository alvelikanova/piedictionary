﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIE_Dictionary.ui.model
{
   public class DictionaryConformity
    {
        private int number;
        private string language;
        private string phorma;
        private string p_00;
        private string p_1;
        private string p_2;
        private string p_3;
        private string p_4;
        private string p_5;
        private string p_6;
        private string p_7;
        private string p_8;
        private string p_9;
        private string p_10;
        private string p_11;
        private string p_0;
        private string meaning;
        private string comment;
        public int Number
        {
            get
            {
                return number;
            }

            set
            {
                number = value;
            }
        }
        public string Language
        {
            get
            {
                return language;
            }
            set
            {
                language = value;
            }
        }
        public string Phorma
        {
            get
            {
                return phorma;
            }
            set
            {
                phorma = value;
            }
        }
        public string P_00
        {
            get
            {
                return p_00;
            }
            set
            {
                p_00 = value;
            }
        }
        public string P_1
        {
            get
            {
                return p_1;
            }
            set
            {
                p_1 = value;
            }
        }
        public string P_2
        {
            get
            {
                return p_2;
            }
            set
            {
                p_2 = value;
            }
        }
        public string P_3
        {
            get
            {
                return p_3;
            }
            set
            {
                p_3 = value;
            }
        }
        public string P_4
        {
            get
            {
                return p_4;
            }
            set
            {
                p_4 = value;
            }
        }
        public string P_5
        {
            get
            {
                return p_5;
            }
            set
            {
                p_5 = value;
            }
        }
        public string P_6
        {
            get
            {
                return p_6;
            }
            set
            {
                p_6 = value;
            }
        }
        public string P_7
        {
            get
            {
                return p_7;
            }
            set
            {
                p_7 = value;
            }
        }
        public string P_8
        {
            get
            {
                return p_8;
            }
            set
            {
                p_8 = value;
            }
        }
        public string P_9
        {
            get
            {
                return p_9;
            }
            set
            {
                p_9 = value;
            }
        }
        public string P_10
        {
            get
            {
                return p_10;
            }
            set
            {
                p_10 = value;
            }
        }
        public string P_11
        {
            get
            {
                return p_11;
            }
            set
            {
                p_11 = value;
            }
        }
        public string P_0
        {
            get
            {
                return p_0;
            }
            set
            {
                p_0 = value;
            }
        }
        public string Meaning
        {
            get
            {
                return meaning;
            }
            set
            {
                meaning = value;
            }
        }
        public string Comment
        {
            get
            {
                return comment;
            }
            set
            {
                comment = value;
            }
        }

        public bool Regular { get; set; }
    }
}