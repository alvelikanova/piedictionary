﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using PIE_Dictionary.ui.model;
using PIE_Dictionary.data;
using PIE_Dictionary.services;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace PIE_Dictionary.ui.components
{

    public partial class VocabularyEntryWindow : Window
    {
        private PIEDataService pieDataService;
        private VocabularyEntry vocabularyEntry;
        public VocabularyEntryWindow(VocabularyEntry vocabularyEntry)
        {
            InitializeComponent();
            pieDataService = new PIEDataService();
            this.vocabularyEntry = vocabularyEntry;
            PIELemmaValueLabel.Content = vocabularyEntry.Lemma;
            PIENumberValueLabel.Content = vocabularyEntry.Number;
            FillDataGrid();
        }

        private void FillDataGrid()
        {
            conformitiesDataGrid.Items.Clear();
            IEnumerable<Dictionary_conformity> entries = pieDataService.LoadDictionaryConformities(vocabularyEntry.Number);
            if (entries != null)
            {
                ObservableCollection<DictionaryConformity> dictionaryConformitiesCollection = new ObservableCollection<DictionaryConformity>();
                foreach (Dictionary_conformity entry in entries)
                {
                    if (entry.Number_string.HasValue)
                    {
                        int number = entry.Number_string.Value;
                        string languageSignature = null;
                        Language language = entry.Language;
                        if (language != null)
                        {
                            languageSignature = language.Sign;
                        }
                        string meaning = entry.Meaning;
                        string phorma = entry.Phorma;
                        string p_00 = entry.p_00;
                        string p_1 = entry.p_1;
                        string p_2 = entry.p_2;
                        string p_3 = entry.p_3;
                        string p_4 = entry.p_4;
                        string p_5 = entry.p_5;
                        string p_6 = entry.p_6;
                        string p_7 = entry.p_7;
                        string p_8 = entry.p_8;
                        string p_9 = entry.p_9;
                        string p_10 = entry.p_10;
                        string p_11 = entry.p_11;
                        string p_0 = entry.p_0;
                        string comment = entry.Comment;
                        dictionaryConformitiesCollection.Add(new DictionaryConformity { Comment = comment, Language = languageSignature, Meaning = meaning, Number = number, Phorma = phorma, P_0 = p_0, P_00 = p_00, P_1 = p_1, P_10 = p_10, P_11 = p_11, P_2 = p_2, P_3 = p_3, P_4 = p_4, P_5 = p_5, P_6 = p_6, P_7 = p_7, P_8 = p_8, P_9 = p_9 });
                    }
                }
                conformitiesDataGrid.DataContext = dictionaryConformitiesCollection;
            }
        }
    }
}