﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PIE_Dictionary.services;
using PIE_Dictionary.data;
using System.Collections.ObjectModel;

namespace PIE_Dictionary.ui.components
{

    public partial class VocabularySearchWindow : Window
    {
        private PIEDataService pieDataService;
        public VocabularySearchWindow()
        {
            InitializeComponent();
            pieDataService = new PIEDataService();
            FillLanguageCombobox();
        }
        private void FillLanguageCombobox()
        {
            ObservableCollection<string> languageList = new ObservableCollection<string>();
            IEnumerable<Language> languages = pieDataService.LoadLanguages();
            if (languages != null)
            {
                foreach (Language language in languages)
                {
                    languageList.Add(language.Name);
                }
            }
            languageSearchField.ItemsSource = languageList;
        }
    }
}
